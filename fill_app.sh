#! /bin/bash
MYDIR=`dirname $0`
cp -fv $MYDIR/template/App.js $MYDIR/src/App.js
BUILD_TIME=`date +%Y-%m-%d_%H:%M:%S`
sed -i "s/+++/$1/g" $MYDIR/src/App.js
sed -i "s/---/$2/g" $MYDIR/src/App.js
sed -i "s/###/$BUILD_TIME/g" $MYDIR/src/App.js
